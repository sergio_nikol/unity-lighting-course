﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {

	[SerializeField]
	private Transform _target;

	private float 	  _difference; 

	// Use this for initialization
	void Start () {
		_difference = transform.position.z - _target.position.z;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 newPos = new Vector3(_target.position.x,transform.position.y,_target.position.z + _difference);
		transform.position = newPos;
	}
}
